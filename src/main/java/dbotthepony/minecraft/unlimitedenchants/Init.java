
// 
// Copyright (C) 2017 DBot
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//      http://www.apache.org/licenses/LICENSE-2.0
//  
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 

package dbotthepony.minecraft.unlimitedenchants;

import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.enchanting.EnchantmentLevelSetEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Mod(modid = Init.MOD_ID, name = Init.MOD_NAME, version = "1.0")
public class Init {
	public static final String MOD_ID =					"unlimitedenchants";
	public static final String MOD_NAME =				"Unlimited Enchantements";
	public static final boolean ENABLE = true;
	
	// Replace original method in a soft way
	public static int calcItemStackEnchantability(Random rand, int enchantNum, int power, ItemStack stack) {
		Item item = stack.getItem();
		int i = item.getItemEnchantability(stack);

		if (i <= 0) {
			return 0;
		} else {
			int j = rand.nextInt(8) + 1 + (power >> 1) + rand.nextInt(power + 1);
			return enchantNum == 0 ? Math.max(j / 3, 1) : (enchantNum == 1 ? j * 2 / 3 + 1 : Math.max(j, power * 2));
		}
    }
	
	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		
	}
	
	@Mod.EventHandler
	public void init(FMLInitializationEvent event) {
		MinecraftForge.EVENT_BUS.register(Init.class);
	}
	
	@Mod.EventHandler
	public void init(FMLPostInitializationEvent event) {
		
	}
	
	@SubscribeEvent
    public static void onEnchantmentLevelSet(EnchantmentLevelSetEvent event) {
		int enchantNum = event.getEnchantRow();
		int power = (int) (event.getPower() * 1.4);
		ItemStack stack = event.getItem();
		BlockPos pos = event.getPos();
		World world = event.getWorld();
		Block block = world.getBlockState(pos).getBlock();
		System.out.print(block);
		
		Random rand = new Random();
		
		rand.setSeed((long) 0);
		
		int calculated = calcItemStackEnchantability(rand, enchantNum, power, stack);
		event.setLevel(calculated);
    }
}
